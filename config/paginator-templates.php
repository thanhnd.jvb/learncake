<?php
return $config = [
    'nextActive' => '<li class="pager__item pager__item--next"><a class="pager__link" href="#">
                        <svg xmlns="http://www.w3.org/2000/svg" width="8" height="12" viewbox="0 0 8 12">
                            <g fill="none" fill-rule="evenodd">
                                <path fill="#33313C" d="M7.41 1.41L6 0 0 6l6 6 1.41-1.41L2.83 6z"></path>
                            </g>
                        </svg>
                     </a></li>',
    'nextDisabled' => '',
    'prevActive' => '<li class="pager__item pager__item--prev"><a class="pager__link" href="{{url}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="8" height="12" viewbox="0 0 8 12">
                            <g fill="none" fill-rule="evenodd">
                                <path fill="#33313C" d="M7.41 1.41L6 0 0 6l6 6 1.41-1.41L2.83 6z"></path>
                            </g>
                        </svg>
                     </a></li>',
    'prevDisabled' => '',
//    'counterRange' => '',
//    'counterPages' => '',
    'first' => '<li class="pager__item"><a class="pager__link" href="{{url}}">First</a></li>',
    'last' => '<li class="pager__item"><a class="pager__link" href="{{url}}">Last</a></li>',
    'number' => '<li class="pager__item"><a class="pager__link" href="{{url}}">{{text}}</a></li>',
    'current' => '<li class="pager__item active"><a class="pager__link" href="">{{text}}</a></li>',
//    'ellipsis' => '',
//    'sort' => '',
//    'sortAsc' => '',
//    'sortDesc' => '',
];
/*
 * Default config
 * */
//            'nextActive' => '<li class="next"><a rel="next" href="{{url}}">{{text}}</a></li>',
//            'nextDisabled' => '<li class="next disabled"><a href="" onclick="return false;">{{text}}</a></li>',
//            'prevActive' => '<li class="prev"><a rel="prev" href="{{url}}">{{text}}</a></li>',
//            'prevDisabled' => '<li class="prev disabled"><a href="" onclick="return false;">{{text}}</a></li>',
//            'counterRange' => '{{start}} - {{end}} of {{count}}',
//            'counterPages' => '{{page}} of {{pages}}',
//            'first' => '<li class="first"><a href="{{url}}">{{text}}</a></li>',
//            'last' => '<li class="last"><a href="{{url}}">{{text}}</a></li>',
//            'number' => '<li><a href="{{url}}">{{text}}</a></li>',
//            'current' => '<li class="active"><a href="">{{text}}</a></li>',
//            'ellipsis' => '<li class="ellipsis">&hellip;</li>',
//            'sort' => '<a href="{{url}}">{{text}}</a>',
//            'sortAsc' => '<a class="asc" href="{{url}}">{{text}}</a>',
//            'sortDesc' => '<a class="desc" href="{{url}}">{{text}}</a>',
//            'sortAscLocked' => '<a class="asc locked" href="{{url}}">{{text}}</a>',
//            'sortDescLocked' => '<a class="desc locked" href="{{url}}">{{text}}</a>',
