<?php
use Cake\Routing\Router;
$session = $this->getRequest()->getSession()->read('Auth.User');
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('mystyle.css') ?>
    <?= $this->Html->css('pagination.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <?php if (!empty($session)): ?>
    <div class="col-md-12">
        <div class="pull-right">
            <span><?= (isset($session["user"]["name"]) ? $session["user"]["name"] : '') ?> | </span>
            <a href="<?= Router::url(['controller' => 'Users', 'action' => 'logout']) ?>">Đăng xuất</a>
        </div>
        <hr>
    </div>
    <?php endif; ?>
    <?= $this->Flash->render() ?>
    <div class="container">
        <?= $this->fetch('content') ?>
    </div>
</body>
</html>
