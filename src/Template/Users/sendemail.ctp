<?php
$this->Blocks->set('title', $title);
$this->layout = 'layout';
?>
<?= $this->Form->create() ?>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label col-md-2">Subject:</label>
            <div class="col-md-10">
                <?= $this->Form->text('subject', ['class' => 'form-control']) ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">To:</label>
            <div class="col-md-10">
                <?= $this->Form->text('to', ['class' => 'form-control']) ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">CC:</label>
            <div class="col-md-10">
                <?= $this->Form->text('cc', ['class' => 'form-control']) ?>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-2">Message:</label>
            <div class="col-md-10">
                <?= $this->Form->textarea('message', ['class' => 'form-control']) ?>
            </div>
        </div>
        <div class="col-md-12 m-t">
            <div class="pull-right">
                <?= $this->Form->button('Gửi', ['class' => 'btn btn-success', 'name' => 'btnSend', 'value' => 'ok']) ?>
            </div>
        </div>
    </div>
<?= $this->Form->end() ?>
