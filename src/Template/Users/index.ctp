<?php
use Cake\Cache\Cache;
use Cake\Core\Plugin;
use Cake\Error\Debugger;

$this->Blocks->set('title', $title);
$this->layout = 'layout';
$page = $this->request->getQuery('page');
$perPage = $this->request->getParam('paging.User.perPage');
if (empty($page)){
    $stt = 1;
}
else{
    $stt = $perPage * $page + 1;
}
?>
<div class="col-md-12">
	<div class="pull-right">
		<a href="<?= $urlAdd ?>" class="btn btn-primary">Thêm user</a>
	</div>
</div>
<div class="col-md-12 m-t">
	<table class="table table-bordered" style="font-size: 20px;">
		<thead>
			<th class="text-center" width="5%">STT</th>
			<th class="text-center"><?= $this->Paginator->sort('name', 'Tên') ?></th>
			<th class="text-center"><?= $this->Paginator->sort('birth', 'Ngày sinh') ?></th>
			<th class="text-center"><?= $this->Paginator->sort('sex', 'Giới tính') ?></th>
			<th class="text-center"><?= $this->Paginator->sort('username', 'Tên tài khoản') ?></th>
			<th class="text-center" width="10%">Tác vụ</th>
		</thead>
		<?php if (!empty($listUser)): ?>
			<tbody>
				<?php foreach ($listUser as $k => $v): ?>
					<tr>
						<td class="text-center"><?= $stt++ ?></td>
						<td><?= $v->name ?></td>
						<td class="text-center"><?= date('d/m/Y', strtotime($v->birth)) ?></td>
						<td class="text-center"><?= $v->sex ?></td>
						<td><?= (!empty($v->login) ? $v->login->username : 'Chưa có tài khoản') ?></td>
						<td class="text-center">
							<a href="<?= $urlAdd."?id=".$v->id; ?>" class="btn btn-xs btn-warning">Sửa</a>
							<?= $this->Form->postLink('Xoá', ['controller' => 'Users', 'action' => 'delete', $v->id], ['inline' => true, 'class' => 'btn btn-xs btn-danger', 'confirm' => __('Are you sure you want to delete')]); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		<?php endif; ?>
	</table>
    <?= $this->Paginator->first(); ?>
    <?= $this->Paginator->prev(); ?>
    <?= $this->Paginator->numbers(); ?>
    <?= $this->Paginator->next(); ?>
    <?= $this->Paginator->last(); ?>
</div>