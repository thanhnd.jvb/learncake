<?php
$this->Blocks->set('title', $title);
$this->layout = 'layout';
$session = $session->read('Auth.User');
$stt = 1;
?>
<?= $this->Form->create() ?>
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
                <th class="text-center" width="5%">STT</th>
                <th class="text-center">Key</th>
                <th class="text-center">Value</th>
                <th class="text-center">Tác vụ</th>
            </thead>
            <tbody>
                <?php foreach ($session as $k => $v): ?>
                    <tr>
                        <td class="text-center"><?= $stt++ ?></td>
                        <td class="text-center"><?= $k ?></td>
                        <td><?php print_r($v); ?></td>
                        <td class="text-center">
                            <button type="submit" class="btn btn-xs btn-danger" name="deletesession" value="<?= $k ?>" onclick="return confirm('Bạn chắc chắn muốn xoá session này');">Xoá</button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-12 m-t">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Key</label>
                <?= $this->Form->text('key', ['class' => 'form-control']) ?>
            </div>
            <div class="form-group">
                <label class="control-label">Value</label>
                <?= $this->Form->text('value', ['class' => 'form-control']) ?>
            </div>
        </div>
        <div class="col-md-6">
            <?= $this->Form->button('Thêm', ['class' => 'btn btn-success',
                                                'name' => 'btnAdd',
                                                'value' => 'ok']) ?>
            <?= $this->Form->button('Huỷ toàn bộ session', ['class' => 'btn btn-primary',
                                                            'name' => 'btnDestroy',
                                                            'value' => 'ok',
                                                            'confirm' => __('Huỷ toàn bộ session ở đây sẽ tự đăng nhập lại bằng cookie(nếu có). Nếu muốn Logout vui lòng ấn "Đăng xuất". Bạn có muốn tiếp tục?')]) ?>
        </div>
    </div>
<?= $this->Form->end() ?>
