<?php
$this->layout = 'layout';
$this->Blocks->set('title', $title);
?>
<div class="users form">
<?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Login') ?></legend>
        <?= $this->Form->control('username') ?>
        <?= $this->Form->control('password') ?>
        <?= $this->Form->checkbox('remember_me') ?> Remember me
   </fieldset>
<?= $this->Form->button(__('Login')); ?>
<?= $this->Form->end() ?>
</div>
