<?php
use Cake\Cache\Cache;
use Cake\Core\Plugin;
use Cake\Error\Debugger;

$this->Blocks->set('title', $title);
$this->layout = 'layout';
?>
<div class="col-md-12">
	<?= $this->Form->create(null, $actionArray) ?>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label">Họ tên</label>
			<?= $this->Form->text('name', ['class' => 'form-control',
										'value' => (isset($info->name) ? $info->name : '')]) ?>
		</div>
		<div class="form-group">
			<label class="control-label">Ngày sinh</label>
			<?= $this->Form->text('birth', ['class' => 'form-control',
											'type' => 'date',
											'value' => (isset($info->birth) ? $info->birth : (date('Y-m-d')))]) ?>
		</div>
		<div class="form-group">
			<label class="control-label">Giới tính</label>
			<?= $this->Form->text('sex', ['class' => 'form-control',
										'value' => (isset($info->sex) ? $info->sex : '')]) ?>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label class="control-label">Tên tài khoản</label>
			<?= $this->Form->text('username', ['class' => 'form-control',
										'value' => (isset($info->login->username) ? $info->login->username : '')]) ?>
		</div>
		<div class="form-group">
			<label class="control-label">Mật khẩu</label>
			<?= $this->Form->password('password', ['class' => 'form-control']) ?>
		</div>
	</div>
	<div class="col-md-12">
		<div class="pull-right">
			<?= $this->Form->button((!empty($info)? 'Sửa' : 'Thêm'), ['class' => 'btn btn-success']) ?>
		</div>
	</div>
	<?= $this->Form->end() ?>
</div>