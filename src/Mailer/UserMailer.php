<?php
namespace App\Mailer;

use Cake\Mailer\Email;
use Cake\Mailer\Mailer;
use Cake\Mailer\TransportFactory;

/**
 * User mailer.
 */
class UserMailer extends Mailer
{
    /**
     * Mailer's name.
     *
     * @var string
     */
    public static $name = 'User';

    public function __construct(Email $email = null)
    {
        parent::__construct($email);
        TransportFactory::setConfig('gmail', [
            'host' => 'smtp.gmail.com',
            'port' => 587,
            'username' => 'thanhnd.jvb@gmail.com',
            'password' => '',
            'className' => 'Smtp',
            'tls' => true
        ]);
    }

    public function sendMyEmail($subject, $to, $cc, $mess)
    {
        $this->transport('gmail')
            ->emailFormat('html')
            ->template('default');
        $this->subject($subject);
        $this->setViewVars(['content' => $mess]);
        if (!empty($to))
        {
            $to = explode(",", str_replace(" ", "", $to));
            $this->to($to);
        }
        if (!empty($cc))
        {
            $cc = explode(",", str_replace(" ", "", $cc));
            $this->cc($cc);
        }
    }
}
