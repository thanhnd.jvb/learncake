<?php


namespace App\Error;


use Cake\Error\ExceptionRenderer;
use Exception;

class AppExceptionRenderer extends ExceptionRenderer
{
    protected function _template(Exception $exception, $method, $code)
    {
        if ($code == 404){
            $template = 'error404';
            return $this->template = $template;
        }
        // khác 404 trả về default
        return parent::_template($exception, $method, $code);
    }
}
